import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Customer  {

    String custName;
    int custId;
    int pass;
    List<Books> books;

    public Customer(String n, int id, int p) {
        this.custName = n;
        this.custId = id;
        this.pass = p;
    }
    Customer(){
        books = new ArrayList<>();
    }

    public void setName(String n) {
        this.custName = n;
    }

    public void setId(int i) {
        this.custId = i;
    }

    public void setPass(int pa) {
        this.pass = pa;
    }

    public String getName() {
        return custName;
    }

    public int getId() {
        return custId;
    }

    public void viewAllBooks(List<Books> books) {
        // System.out.println("_______________________________________________________");
        books.forEach((book) -> {
            System.out.println("Book Id: " + book.getBookid() + " Book Name: " + book.getBookname());
        });
       
    }

    public void viewBookDetails(Books book) {
        System.out.println("_______________________________________________________");
        System.out.println("Book Id: " + book.getBookid() + "\nBook Name: " + book.getBookname() + "\nAuthor Name: "
                + book.getAuthorname() + "\nNumber Of Books Available: " + book.getQty());
    }

    public void returnBook(Books book) {
        // books.remove(book);
        System.out.println("Book: " + book.getBookname() + " Returned.");
    }

    public String requestForBook(String bookName){
        System.out.println("Request has been sent successfully.....");
        return bookName;
    }
    public void integrate(List<Books> books) {
        Scanner sc = new Scanner(System.in);
        int ch;

        do {
            System.out.println("_______________________________________________________");
            System.out.println("1. View All Books\n2. View Book Details\n3. Return Book\n4. Request Book\n5. Exit");
            System.out.print("Enter your choice: ");
            ch = sc.nextInt();

            switch (ch) {
                case 1:
                    viewAllBooks(books);
                    break;
                case 2:
                    System.out.print("Enter the book name: ");
                    String bookName = sc.next();
                    Books foundBook = null;
                    for (Books book : books) {
                        if (book.getBookname().equalsIgnoreCase(bookName)) {
                            foundBook = book;
                            break;
                        }
                    }
                    if (foundBook != null) {
                        viewBookDetails(foundBook);
                    } else {
                        System.out.println("Book not found!");
                    }
                    break;
                case 3:
                    System.out.println("Enter the book name to return: ");
                    String returnBookName = sc.next();
                    Books returnBook = null;
                    for (Books book : books) {
                        if (book.getBookname().equalsIgnoreCase(returnBookName)) {
                            returnBook = book;
                            break;
                        }
                    }
                    if (returnBook != null) {
                        returnBook(returnBook);
                    } else {
                        System.out.println("Book not found!");
                    }
                    break;
                case 4:
                     System.out.println("Enter the book name for request: ");
                    String requestBooKName= sc.next();
                    Books requestBook = null;
                    for (Books book : books) {
                        if (book.getBookname().equalsIgnoreCase(requestBooKName)) {
                            requestBook = book;
                            break;
                        }
                    }
                    if (requestBook != null) {
                        requestForBook(requestBooKName);
                        // System.out.println("Request has been sent successfully.....");
                    } else {
                        System.out.println("Book not found!");
                    }
                    break;
                case 5:
                        break;
                default:
                    System.out.println("Invalid Entry");
            }

        } while (ch != 5);

        sc.close();
    }

    // public static void main(String[] args) {
    //     List<Books> books = new ArrayList<>();
    //     books.add(new Books("Book1", "Author1", 1234, 5));
    //     books.add(new Books("Book2", "Author2", 5678, 3));
    //     books.add(new Books("Book3", "Author3", 9012, 4));
    //     Customer customer1=new Customer("Baganna", 123, 1199);
    //     // customer1.integrate(books);
    //     customer1.requestForBook("book2");


    // }
}

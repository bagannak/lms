
public class Books {
    String bookName, authorName;
    int bookid, qty;

    public Books(String bn, String an, int bi, int q){
        this.bookName = bn;
        this.authorName = an;
        this.bookid = bi;
        this.qty = q;
    }

    public void setBookname(String b){
        this.bookName = b;
    }

    public void setAuthorname(String a){
        this.authorName = a;
    }

    public void setBookid(int i){
        this.bookid = i;
    }
    public void setQty(int q){
        this.qty = q;
    }

    public String getBookname(){
        return bookName;
    }

    public String getAuthorname(){
        return authorName;
    }

    public int getBookid(){
        return bookid;
    }

    public int getQty(){
        return qty;
    }

}

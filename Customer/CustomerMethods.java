
import java.util.List;

public interface CustomerMethods{
    void viewAllBooks(List<Books> books);
    void viewBookDetails(Books book);
    void returnBook(Books book);
    String requestForBook(String bookName);
} 
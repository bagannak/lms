import java.util.ArrayList;
import java.util.Scanner;
public class Login {
    public static void main(String[] args) {
        Admin a = new Admin();
        Customer c = new Customer("baganna",123,34);
    
        Scanner sc = new Scanner(System.in);
        int r;
        do {
            ArrayList<Books> b = new ArrayList<>();
            ArrayList<Customer> customers = new ArrayList<>();
            System.out.println("______MENU_________");
            System.out.println("1. Admin");
            System.out.println("2. Customer");
            System.out.println("Enter your role (1 for Admin, 2 for Customer, 3 to exit): ");
             b.add(new Books("Book1", "Author1", 1234, 5));
             b.add(new Books("Book2", "Author2", 5678, 3));
             b.add(new Books("Book3", "Author3", 9012, 4));
            c.viewAllBooks(b);
            r = sc.nextInt();
            switch (r) {
                case 1:
                    a.integrate(b,customers);
                    break;
                case 2:
                    c.integrate(b);
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Invalid choice");
            }
        } while (r != 3);
        sc.close();
        
    }
}
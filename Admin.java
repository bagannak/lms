import java.util.*;
import java.util.Scanner;

public class Admin{

    ArrayList<Books> bo = new ArrayList<>();
    ArrayList<Customer> cust = new ArrayList<>();
    Scanner sc = new Scanner(System.in);
    Customer c = new Customer();

    public void integrate(List<Books> bo,List<Customer> customers) {
        int op;
        do {
            System.out.println("_______________________________________");
            System.out.println("");
            System.out.println(".....MENU.........");
            System.out.println("1. customer operations");
            System.out.println("2. books operations");
            System.out.println("3. Exit");
            System.out.println("Enter your choice:");
            System.out.println("______________________________________");
            op = sc.nextInt();
            
            switch (op) {
                case 1:
                    custop(customers);
                    break;
                case 2:
                    bookop(bo);
                    break;
                case 3:
                    System.out.println("Exiting the program. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        } while (op != 3);
    }

       

    public void bookop(List<Books> bo){
        System.out.println("___________________________________");
        System.out.println("            ");
        System.out.println("1. Add books");
        System.out.println("2. View books");
        System.out.println("3. Edit books");
        System.out.println("4. Remove books");
        System.out.println("5. Issue books to customer");
        System.out.println("6. Exit");
        System.out.println("Enter your choice of operation:");
        System.out.println("______________________________________");
        int ch = sc.nextInt();

        switch (ch) {
            case 1:
                addBook(bo);
                break;
            case 2:
                showBooks(bo);
                break;
            case 3:
                editBooks(bo);
                break;
            case 4:
                removeBooks(bo);
                break;
            case 5:
                issueBook(null);
                break;
            case 6:
                System.out.println("Exiting the program. Goodbye!");
                break;
            default:
                System.out.println("Invalid choice. Please try again.");
                break;
        }
    }

 public void custop(List<Customer> bo){
            int ch;
            System.out.println("_________________________");
            System.out.println("               ");
            System.out.println("1. Add a customer");
            System.out.println("2. View all customers");
            System.out.println("3. Edit a customer");
            System.out.println("4. Remove a customer");
            System.out.println("____________________________");
            ch = sc.nextInt();

            switch (ch) {
                case 1:
                    addCustomer(bo);
                    break;
                case 2:
                    viewCustomer(bo);
                    break;
                case 3:
                    editCustomer(bo);
                    break;
                case 4:
                    removeCustomer(bo);
                    break;
                default:
                    System.out.println("invalid");
                    break;
            }
        }
/*customer operations
    add
    view
    edit
    remove
*/
        public void addCustomer(List<Customer> bo) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Customer ID: ");
            int id = sc.nextInt();

            for (Customer customer : cust) {
                if (customer.getId() == id) {
                    System.out.println("ID already exists.");
                    return;
                }
            }
            System.out.println("Customer name: ");
            String name = sc.next();

            System.out.println("password : ");
            int pass = sc.nextInt();

            Customer c = new Customer(name, id, pass);
            cust.add(c);
        }



    public void viewCustomer(List<Customer> bo){
        for(int i=0;i<cust.size();i++){
            System.out.println("_________________________");
            System.out.println("Id : "+cust.get(i).getId());
            System.out.println("Name : "+cust.get(i).getName());
            System.out.println("_______________________");
        }
    }

    public void editCustomer(List<Customer> bo) {
        for (int i = 0; i < cust.size(); i++) {
            System.out.println("_______________________________");
            System.out.println((i + 1));
            System.out.println("Id: " + cust.get(i).getId());
            System.out.println("Name: " + cust.get(i).getName());
            System.out.println("__________________________");
        }
        System.out.println("Enter the index of the customer you want to edit: ");
        int choice = sc.nextInt();

        if (choice > 0 && choice <= cust.size()) {
            System.out.println("Enter the new ID: ");
            int id = sc.nextInt();

            for (Customer customer : cust) {
                if (customer.getId() == id) {
                    System.out.println("ID already exists.");
                    return;
                }
            }

            System.out.println("Enter the new name: ");
            String name = sc.next();

            Customer customer = cust.get(choice - 1);
            customer.setId(id);
            customer.setName(name);
        } else {
            System.out.println("Invalid choice. Please try again.");
        }
    }

        public void removeCustomer(List<Customer> bo) {
            int index = 1;
            for (int i = 0; i < cust.size(); i++) {
                System.out.println(index + " " + cust.get(i).getName());
                index++;
            }
            System.out.println("Enter the index which you want to delete: ");
            int ind = sc.nextInt();
            if (ind > 0 && ind <= cust.size()) {
                cust.remove(ind - 1);
                System.out.println("Removed successfully.");
            }
        }

        public void issueBook(String b){
            int qu;
            for(int i=0;i<bo.size();i++){
                if(b == bo.get(i).getBookname()){
                    if(bo.get(i).getQty() > 0){
                        System.out.println("issued");
                        qu = bo.get(i).getQty();
                        qu--;
                    }
                }
            }
        }


//customer operations ended


/*books operations
    add
    view
    edit
    remove

*/

    public void addBook(List<Books> bo){
            Scanner sc = new Scanner(System.in);
            System.out.println("Book ID: ");
            int id = sc.nextInt();

            for (Books book : bo) {
                if (book.getBookid() == id) {
                    System.out.println("ID already exists.");
                    return;
                }
            }

            System.out.println("Book name: ");
            String name = sc.next();

            System.out.println("Author name:");
            String aname = sc.next();

            System.out.println("Available number of copies : ");
            int qty = sc.nextInt();

            Books b = new Books(name, aname, id, qty);
            bo.add(b);

    }

    public void showBooks(List<Books> bo){
        for(int i=0;i<bo.size();i++){
            System.out.println("_______________________");
            System.out.println("book-id : "+bo.get(i).getBookid());
            System.out.println("book-name : "+bo.get(i).getBookname());
            System.out.println("author-name : "+bo.get(i).getAuthorname());
            System.out.println("qty available : "+bo.get(i).getQty());
            System.out.println("_____________________________");
        }
    }

            public void editBooks(List<Books> bo) {
                for (int i = 0; i < bo.size(); i++) {
                    System.out.println((i + 1));
                    System.out.println("Id : " + bo.get(i).getBookid());
                    System.out.println("Book Name : " + bo.get(i).getBookname());
                    System.out.println("Author Name : " + bo.get(i).getAuthorname());
                    System.out.println("Qty available : " + bo.get(i).getQty());

                    System.out.println("________________________");
                }

                

                System.out.println("Enter the index of the book you want to edit: ");
                int ch = sc.nextInt();
                if (ch > 0 && ch <= bo.size()) {
                    Books book = bo.get(ch - 1);

                    System.out.println("Enter the new book ID: ");
                    int newId = sc.nextInt();

                    for (Books b : bo) {
                        if (b.getBookid() == newId && b != book) {
                            System.out.println("ID already exists. Cannot update.");
                            return;
                        }
                    }

                    System.out.println("Enter the new book name: ");
                    String newName = sc.next();
                    System.out.println("Enter the new author name: ");
                    String newAuthor = sc.next();
                    System.out.println("Enter the new quantity: ");
                    int newQty = sc.nextInt();

                    book.setBookid(newId);
                    book.setBookname(newName);
                    book.setAuthorname(newAuthor);
                    book.setQty(newQty);

                    System.out.println("Book details updated successfully.");
                } else {
                    System.out.println("Invalid index. Please try again.");
                }
            }

            public void removeBooks(List<Books> bo) {
                for (int i = 0; i < bo.size(); i++) {
                    System.out.println((i + 1));
                    System.out.println("Id : " + bo.get(i).getBookid());
                    System.out.println("Book Name : " + bo.get(i).getBookname());
                    System.out.println("Author Name : " + bo.get(i).getAuthorname());
                    System.out.println("Qty available : " + bo.get(i).getQty());
                    System.out.println("_________________________");
                }

                System.out.println("Enter the index of the book you want to delete: ");
                int s = sc.nextInt();
                if (s > 0 && s <= bo.size()) {
                    bo.remove(s - 1);
                    System.out.println("Book removed successfully.");
                } else {
                    System.out.println("Invalid index. Please try again.");
                }
            }



            public boolean login(int choice) {
                return false;
            }



}

//books operation ended



class Admindetails{
    private int adminId;
    private String adminName;
    private String pass;

    public Admindetails(int id,String na,String p){
        this.adminId = id;
        this.adminName = na;
        this.pass = p;
    }

    public int getId(){
        return adminId;
    }

    public String getName(){
        return adminName;
    }

    public String getPass(){
        return pass;
    }

    public void setId(int i){
        this.adminId = i;
    }

    public void setName(String n){
        this.adminName = n;
    }

    public void setPass(String pa){
        this.pass = pa;
    }



}